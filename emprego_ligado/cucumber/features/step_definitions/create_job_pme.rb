# encoding: utf-8
#!/usr/bin/env ruby

Then(/^company create the new job pme$/) do
  ### Create and aprove Job
    job = RegisterCompany.new
    find(:css, 'a.vacancy').click
    select 'Vendas', from: 'edit-category'
    sleep 05
    select 'Vendedor', from: 'edit-field-position-und--2'
    #fill_in 'edit-field-alternative-name-und-0-value', with: 'Vaga PME 2'
    select '1 Ano', from: 'edit-field-job-experience-und'
    select 'Escala', from: 'edit-shift-type'
    select '5x2', from: 'edit-shift-work-days-ratio'
    select 'Diversos turnos disponiveis', from: 'edit-shift-hours-type'
    select 'CLT', from: 'edit-contract-type'
    #select 'Autônomo', from: 'edit-contract-type'
    ### Salary to combine
    find(:radio_button, 'edit-salary-type-3').set(true)
    ### Beneficts (Some)
    all(:css, '.form-checkbox').map(&:click)
    ### Level of Education
    select 'Médio completo', from: 'edit-field-job-education-und'
    find(:checkbox, 'edit-field-job-driver-license-und-a').set(true)
    #fill_in 'edit-field-job-description-und-0-value', with: Faker::Lorem.paragraph(6)
    find(:id, 'edit-next').click
    #fill_in 'edit-field-reference-point-und-0-value', with: 'Referencia Entrevista'
    #fill_in 'edit-interview-contact', with: 'Biro'
    find(:id, 'edit-next').click
    all(:css, '.confirmation-left .confirm-action').map(&:click)
    all(:css, '.confirmation-right .confirm-action').map(&:click)
    click_button 'Adicionar'
    # Aprove on admin #
    #session = Capybara::Session.new(:selenium)
    session = Capybara::Session.new(:webkit)
    session.visit 'https://empregoligado.qa.empregoligado.net/user'
    #session.visit 'https://empregoligado.com.br/user'
    session.fill_in 'edit-name', with: 'root'
    session.fill_in 'edit-pass', with: '123mudar'
    session.click_button ('Entrar')
    session.visit('https://empregoligado.qa.empregoligado.net/admin/users')
    #session.visit('https://empregoligado.com.br/admin/users')
    session.first(:link, "editar").click
    session.click_link 'Employer'
    session.find(:checkbox, 'edit-profile-employer-field-paying-company-und').set(true)
    session.click_button 'Salvar'
    session.visit ('https://empregoligado.qa.empregoligado.net/admin/job/review')
    #session.visit ('https://empregoligado.com.br/admin/job/review')
    session.find(:css, 'a.fieldset-title').click
    session.all(:css, 'div.fieldset-wrapper a')[4].click
    session.find(:css, '#edit-submit.form-submit').click
    sleep 03
end
