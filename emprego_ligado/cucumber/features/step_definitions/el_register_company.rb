# encoding: utf-8
#!/usr/bin/env ruby

### Company Register
And(/^select company for register$/) do
  find(:css, 'a.header-company').click
  find(:css, 'a.hero-sign-up').click
end

And(/^fill out the registration fields for company$/) do
  reg_company = RegisterCompany.new
  reg_company.company_name.set(Faker::Company.name)
  #reg_company.company_name.set("Empresa Overbooking")
  reg_company.company_pme.select("1000+")
  execute_script "jQuery('#edit-profile-employer-field-employer-phone-und-0-value').val('"+Faker::Base.numerify('112419####').to_s+"');"
  reg_company.company_mail.set(@email_company)
  reg_company.company_areapostada.set("Restaurante")
  click_button('Continuar')
  reg_company.company_contact_name.set(Faker::Name.name)
  execute_script "jQuery('#edit-profile-employer-field-cnpj-number-und-0-value').val('"+Faker::CNPJ.number+"');"
  reg_company.company_password.set("inicial1234")
  reg_company.company_password_confirmation.set("inicial1234")
  execute_script "jQuery('#edit-profile-employer-field-company-zip-und-0-value').val('04679-230\t');"
  reg_company.company_address.set("Rua Eurico Leme Ramos")
  reg_company.company_add_number.set("12")
  reg_company.company_add_compl.set("Casa")
  reg_company.company_add_suburb.set("Vila Santana")
  reg_company.company_add_city.set("Sao Paulo")
  find(:css, '#edit-profile-employer-field-monthly-job-average-und').all(:css, 'option')[rand(6)].select_option
  find(:css, '#edit-profile-employer-field-company-type-und').all(:css, 'option')[rand(16)].select_option
  fill_in 'edit-profile-employer-field-company-description-und-0-value', with: Faker::Lorem.paragraph(6)
  #fill_in 'edit-profile-employer-field-company-description-und-0-value', with: 'Pizza Hut é uma cadeia de restaurantes e franquia internacional com sede em Plano, Texas, especializada em pizzas.'
  #reg_company.company_description.set(Faker::Lorem.paragraph(6))
  find(:css, '#edit-profile-employer-field-company-traffic-origin-und').all(:css, 'option')[rand(9)].select_option
  click_button 'Continuar'
  sleep 03
  # Aprove on admin # -- Usar apenas quando rodar criacao de empresa apenas
  #session = Capybara::Session.new(:selenium)
  session = Capybara::Session.new(:webkit)
  session.visit 'https://empregoligado.qa.empregoligado.net/user'
  #session.visit 'https://empregoligado.com.br/user'
  session.fill_in 'edit-name', with: 'root'
  session.fill_in 'edit-pass', with: '123mudar'
  session.click_button ('Entrar')
  session.visit('https://empregoligado.qa.empregoligado.net/admin/users')
  #session.visit('https://empregoligado.com.br/admin/users')
  session.first(:link, "editar").click
  session.click_link 'Employer'
  session.find(:checkbox, 'edit-profile-employer-field-paying-company-und').set(true)
  session.click_button 'Salvar'
end

Then(/^register company with sucess$/) do
  page.has_selector?(:css, "div.messages.status")
  puts "Your email of register is: " + @email_company
end
