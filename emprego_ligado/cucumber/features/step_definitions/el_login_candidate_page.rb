# encoding: utf-8
#!/usr/bin/env ruby

### Login candidate with menu lateral
And (/^I acecess Login Page with menu lateral and I fill out the login fields of candidate$/) do
  find(:css, '.header-hamburguer').click
  sleep 03
  click_link 'Entrar na Conta'
  execute_script "jQuery('#edit-phone-candidate').val('11994431352');"
  fill_in 'edit-password-candidate', with: 'inicial1234'
end

Then(/^login with menu lateral success$/) do
  find(:css, '#edit-login').click
end
