# encoding: utf-8
#!/usr/bin/env ruby

### Candidates Register
Given(/^I accessed the and autenticate$/) do
  #visit 'https://empregoligado.com.br/'
  visit 'http://empregoligado.qa.empregoligado.net/'
  click_button 'Ok'
end

And(/^I fill out the registration fields$/) do
  reg_candidate = RegisterCandidate.new
  click_link 'Quero me cadastrar agora'
  reg_candidate.candidate_name.set(Faker::Name.name)
  #reg_candidate.candidate_name.set("Thiago Biro Jofence")
  execute_script "jQuery('#edit-profile-employee-field-employee-phone-und-0-value').val('"+@phone_number+"');"
  reg_candidate.candidate_pass.set("inicial1234")
end

And(/^click Continuar$/) do
  click_button('Continuar')
  #click_button('Ok') -- Apenas quando for DDD fora de SP
end

And(/^I fill the additional fields$/) do
  reg_candidate = RegisterCandidate.new
  reg_candidate.candidate_email.set(@email_candidate)
  execute_script "jQuery('#edit-profile-employee-field-employee-cpf-und-0-value').val('"+@cpf+"');"
  execute_script "jQuery('#edit-profile-employee-field-employee-zip-und-0-value').val('05433010');"
  reg_candidate.area.select("Administrativo")
  reg_candidate.position.select("Recepcionista")
  reg_candidate.years_exp.set("2")
  reg_candidate.month.select("jan")
  reg_candidate.day.select("10")
  reg_candidate.year.select("1986")
  find(:id, 'edit-profile-employee-field-minimum-salary-und').set(true)
  reg_candidate.education.select("Médio completo")
  reg_candidate.gender.select("Masculino")
  click_button('Continuar')
  puts 'the phone number is: ' + @phone_number
end

And(/^confirm using the code sent$/) do
  session = Capybara::Session.new(:webkit)
  #session = Capybara::Session.new(:selenium)
  visit 'https://empregoligado.qa.empregoligado.net//user'
  #visit 'https://empregoligado.com.br//user'
  session.fill_in 'edit-name', with: 'root'
  session.fill_in 'edit-pass', with: '123mudar'
  session.click_button ('Entrar')
  session.visit('https://empregoligado.qa.empregoligado.net/admin/sms/messages')
  #session.visit('https://empregoligado.com.br/admin/sms/messages')
  @confirmation_code = session.find(:css, "div.view-content .odd.views-row-first .views-field.views-field-message").text.split(': ')[1]
  execute_script "window.close()"
  fill_in 'edit-code', with: @confirmation_code
  click_button('Confirmar')
end

Then(/^candidate registered with sucess$/) do
  expect(page).to have_content 'TELEFONE CONFIRMADO'
end

### Login Candidate Page
And(/^I acecess Login Page and I fill out the login fields of candidate$/) do
  click_link 'Clique aqui'
  execute_script "jQuery('#edit-phone-candidate').val('11994431352');"
  fill_in 'edit-password-candidate', with: 'inicial1234'
end

Then(/^login success$/) do
  find(:css, '#edit-login').click
end

### Login Company Page
And(/^I acecess company login page and I fill out the login fields of candidate$/) do
  click_link 'sou empresa'
  all(:css, 'a.top-header-link.left')[1].click
end

Then(/^company login success$/) do
  fill_in 'edit-username-employer', with: 'theresia@breitenbergcollins.org'
  fill_in 'edit-password-employer', with: 'inicial1234'
  find(:css, '#edit-login--2').click
end

### JOb Existence
And(/^do i log with company existence$/) do
  #click_link 'sou empresa'
  find(:css, '.header-company').click
  all(:css, 'a.top-header-link.left')[1].click
  fill_in 'edit-username-employer', with: 'murray@ritchie.net'
  fill_in 'edit-password-employer', with: 'inicial1234'
  find(:css, '#edit-login--2').click
end
