require "selenium-webdriver"
require 'capybara/cucumber'
require 'faker'
require 'cpf_faker'
require "capybara-webkit"
require 'site_prism'

#Capybara.default_driver = :selenium
Capybara.default_driver = :webkit
Capybara.default_max_wait_time = 10
#Capybara.javascript_driver = :webkit

Capybara::Webkit.configure do |config|
  config.allow_url("https://empregoligado.com.br/empresa")
  config.block_unknown_urls
  config.allow_url "http://empregoligado.qa.empregoligado.net"
  config.allow_url "http://empregoligado.qa.empregoligado.net/user"
  config.allow_url "http://empregoligado.qa.empregoligado.net/empresa"
  config.allow_url "https://empregoligado.qa.empregoligado.net"
  config.allow_url "https://empregoligado.qa.empregoligado.net/user"
  config.allow_url "https://empregoligado.qa.empregoligado.net/empresa"
  config.allow_url "https://empregoligado.com.br"
  config.allow_url "https://empregoligado.com.br/user"
  config.allow_url "https://empregoligado.com.br/empresa"
end
