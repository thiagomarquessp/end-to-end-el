# encoding: utf-8
#!/usr/bin/env ruby

class RegisterCandidate <SitePrism::Page
  element :candidate_name, "input[id='edit-profile-employee-field-employee-name-und-0-value']"
  element :candidate_pass, "input[id='edit-pass']"
  element :area, "select[id='edit-profile-employee-field-desired-cat1-und']"
  element :position, "select[id='edit-profile-employee-field-desired-job1-und']"
  element :years_exp, "input[id='edit-profile-employee-field-years-exp1-und-0-value']"
  element :month, "select[id='edit-profile-employee-field-employee-dob-und-0-value-month']"
  element :day, "select[id='edit-profile-employee-field-employee-dob-und-0-value-day']"
  element :year, "select[id='edit-profile-employee-field-employee-dob-und-0-value-year']"
  element :education, "select[id='edit-profile-employee-field-employee-education-und']"
  element :gender, "select[id='edit-profile-employee-field-employee-sex-und']"
  element :phone, "input[execute_script[id='edit-profile-employee-field-employee-phone-und-0-value']]"
  element :candidate_email, "input[id='edit-profile-employee-mail']"
end

class RegisterCompany <SitePrism::Page
  element :company_name, "input[id='edit-profile-employer-field-company-name-und-0-value']"
  element :company_mail, "input[id='edit-mail']"
  element :company_areapostada, "input[id='edit-category']"
  element :company_contact_name, "input[id='edit-profile-employer-field-contact-name-und-0-value']"
  element :company_password, "input[id='edit-profile-employer-pass']"
  element :company_password_confirmation, "input[id='edit-profile-employer-pass-confirmation']"
  element :company_address, "input[id='edit-profile-employer-field-company-street-name-und-0-value']"
  element :company_add_number, "input[id='edit-profile-employer-field-company-street-number-und-0-value']"
  element :company_add_compl, "input[id='edit-profile-employer-field-company-apt-und-0-value']"
  element :company_add_suburb, "input[id='edit-profile-employer-field-company-suburb-und-0-value']"
  element :company_add_city, "input[id='edit-profile-employer-field-company-city-und-0-value']"
  element :company_number_employees, "select[id='edit-profile-employer-field-company-city-und-0-value']"
  element :company_description, "input['edit-profile-employer-field-company-description-und-0-value']"
  element :company_pme, "select[id='edit-profile-employer-field-company-employees-und']"
end
