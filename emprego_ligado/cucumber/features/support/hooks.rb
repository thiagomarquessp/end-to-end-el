# encoding: utf-8
#!/usr/bin/env ruby

Before do
  @phone_number = Faker::Base.numerify('11994######').to_s
  @email_candidate = Faker::Internet.email
  @cpf = Faker::CPF.numeric
  @email_company = Faker::Internet.email
end

After do
	Capybara.reset_sessions!
end
